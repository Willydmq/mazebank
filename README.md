# Nombre del Proyecto 📋

"**Mazebank:** esta aplicación permite a los clientes de un banco ver sus cuentas y realizar transacciones, como depósitos, retiros y transferencias.". 😀

<div style="text-align: center; padding: 10px; display:flex flex-direction:column">
    <h1 style="font-size:25px; text-decoration-line: underline;">Version Escritorio 💻</h1>
    <div style="display:flex; flex-wrap: wrap; gap:5px; justify-content: center;">
      <img src="./src/main/resources/Images/Dasboard.png" width="300px">
      <img src="./src/main/resources/Images/Clients.png" width="300px">
      <img src="./src/main/resources/Images/Admin.png" width="300px">
    </div>
</div>

# Link Proyecto

<div style="display: flex; flex-direction: column; align-items: center;">
    <img src="./src/main/resources/Images/bank.png" width="50px">
    <a style="color: blue; font-size: 20px; display: block; text-align: center;" href="#" target="_blank">MazeBank</a>
</div>

## Instalación ⚙️

1. Descarga e instalación de IntelliJ IDEA, puede descargar **IntelliJ IDEA** desde el sitio web de JetBrains.

2. Descarga **JavaFX**, es un framework de desarrollo de aplicaciones de escritorio y móviles, puede descargar JavaFX desde el sitio web de Openjfx.

3. Una vez que haya instalado IntelliJ IDEA, abra el IDE y cree un nuevo proyecto Java.

4. Una vez que haya creado un proyecto Java, debe agregar los archivos **JAR de JavaFX** y las **dependencias de SQLite y SLF4J** a las bibliotecas del proyecto. Para hacer esto, siga estos pasos:

   <strong>a.</strong> En el proyecto, haga clic con el botón derecho en el directorio lib.
   <strong>b.</strong> Seleccione Agregar archivos al directorio.
   <strong>c.</strong> Busque los archivos JAR de JavaFX que descargó y selecciónelos.
   <strong>d.</strong> Haga clic en Abrir.

5. Descargue el archivo .db de Mazebank desde la URL proporcionada por **[@forjava437](https://www.youtube.com/@forjava437/featured)**.

   ```
   https://www.dropbox.com/s/vu1rwk200fvu9ne/mazebank.db?dl=0
   ```

6. Una vez que haya descargado el archivo .db, debe agregarlo a su proyecto. Para hacer esto, siga estos pasos:

   <strong>a.</strong> En el proyecto, asegurece en estar en la carpeta principal.
   <strong>b.</strong> Seleccione el archivo .db y arrastelo a la carpeta principal.
   <strong>"a tener en cuenta:"</strong> El archivo .db debe estar afuera de la carpeta SRC.

7. Compile y ejecute el proyecto
   Una vez que haya agregado los archivos JAR de JavaFX, SQLite, SLF4J y el archivo .db al proyecto, puede compilar y ejecutar el proyecto. Para hacer esto, siga estos pasos:

   <strong>a.</strong> En el proyecto, haga clic en el botón Run.
   <strong>b.</strong> El proyecto se compilará y ejecutará.

### Requisitos 📄

1. Conocimiento básico de la programación en **Java**, incluyendo los conceptos de objetos, clases, métodos y variables.

2. Conocimiento básico del framework **JavaFX**, incluyendo los conceptos de escenas, controles y eventos.

3. Debe tener un conocimiento básico de la base de datos **SQLite**, incluyendo los conceptos de tablas, columnas y registros.

4. Conocimiento de diseño de interfaces de usuario: principios de diseño de **interfaces de usuario**, incluyendo la facilidad de uso, la accesibilidad y la usabilidad.

## Uso 💪

Mazebank tiene una interfaz de usuario sencilla y fácil de usar. Los clientes pueden iniciar sesión en la aplicación con sus credenciales de inicio de sesión. Una vez que estén conectados, podrán ver una lista de sus cuentas. Desde allí, pueden realizar transacciones en sus cuentas.

## Construido con 🛠️

<div style="text-align: center; padding: 10px;">
    <img src="./src/main/resources/Images/java.png" width="100px">
    <img src="./src/main/resources/Images/SceneBuilder.png" width="100px">
    <img src="./src/main/resources/Images/sqlite.png" width="100px">
</div>

## Deployment 🚀

La arquitectura de nuestra aplicación consta de tres partes principales: **La capa de presentación** es la capa más externa de la aplicación. Se encarga de mostrar la interfaz de usuario al usuario. La capa de presentación está construida usando JavaFX, un framework de desarrollo de aplicaciones de escritorio y móviles. **La capa de lógica de negocio** es la capa intermedia de la aplicación. Se encarga de la lógica de la aplicación. La capa de lógica de negocio está construida usando Java. **La capa de datos** es la capa más interna de la aplicación. Se encarga de la interacción con la base de datos. La capa de datos está construida usando SQLite, una base de datos relacional de código abierto.

## Autores ✒️

- **William Maldonado** - _Challenge: JavaFX - Create Banking Application With Data Persistence - 2022_ - [Willydmq](https://gitlab.com/Willydmq)

## Expresiones de Gratitud

- Me gustaría expresar mi más sincero agradecimiento a **[@forjava437](https://www.youtube.com/@forjava437/featured)** por todo el esfuerzo y dedicación que ha puesto en el Challenge. 🤓.
- Gracias a sus enseñanzas y el video tutorial de **[YouTube](https://www.youtube.com/watch?v=lkov5shwRQs&t=26700s)**, pude aprender nuevas habilidades y poner en práctica mis conocimientos para construir esta aplicacion desktop. 📢.
- Estoy muy agradecido por la oportunidad de aprender de un tutor tan talentoso y dedicado, y espero seguir aprendiendo de él en el futuro. ¡Gracias de nuevo, **[@forjava437](https://www.youtube.com/@forjava437/featured)**! 🌟.

---

⌨️ con ❤️ por [William Maldonado](https://gitlab.com/Willydmq) 😊

---
