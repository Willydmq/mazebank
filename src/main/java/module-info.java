module com.jmc.mazebank {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires de.jensd.fx.glyphs.fontawesome;
    requires java.sql;
    requires mysql.connector.java;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires com.almasb.fxgl.all;
//    requires eu.hansolo.tilesfx;


    opens com.jmc.mazebank.mazebank to javafx.fxml;
    exports com.jmc.mazebank.mazebank;
    exports com.jmc.mazebank.mazebank.Controllers;
    exports com.jmc.mazebank.mazebank.Controllers.Admin;
    exports com.jmc.mazebank.mazebank.Controllers.Client;
    exports com.jmc.mazebank.mazebank.Models;
    exports com.jmc.mazebank.mazebank.Views;
}