package com.jmc.mazebank.mazebank.Models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class SavingAccount extends Account {
    private final IntegerProperty withdrawalLimit;

    public SavingAccount(String owner, String accountNumber, double balance, int withdrawalLimit) {
        super(owner, accountNumber, balance);
        this.withdrawalLimit = new SimpleIntegerProperty(this, "Withdrawal Limit", withdrawalLimit);
    }

    public IntegerProperty transactionLimitProp(){
        return withdrawalLimit;
    }
}
